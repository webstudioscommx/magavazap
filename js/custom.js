$(document).ready(function() {
          $( ".secciones" ).hide();
          $( ".secciones.active" ).show();


            $( "a.btn-secciones" ).click(function() {
              var target = "#" + $(this).data("target");

                $("a.btn-secciones img").removeClass('circle-active');
                $(this).find('img').addClass('circle-active');
                $("a.btn-secciones i").removeClass('circle-active');
                $(this).find('i').addClass('circle-active');
                $(".secciones").not(target).hide();
                $(target).show();
                
            });


          $(".button-collapse").sideNav();
          $('.parallax').parallax();
          // $.backstretch("img/video.mp4");
        });